import json
try:
    place_ord = json.load(open("./common/mock/place_order_response.json"))
    holdings = json.load(open("./common/mock/holdings.json"))
    profile = json.load(open("./common/mock/profile.json"))
except IOError:
    print ("Could not read file:")
