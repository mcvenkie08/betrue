from flask import Blueprint
from common.utils import holdings

portfolio = Blueprint("portfolio", __name__)

@portfolio.route("/holdings", methods=["GET"])
def get_holdings():
    return holdings
