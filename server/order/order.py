from flask import Blueprint
from common.utils import place_ord

order = Blueprint("order", __name__)

@order.route("/place_order", methods=["GET"])
def place_order():
    return place_ord
