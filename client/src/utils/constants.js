// export const 

(()=>{
    console.log(process.env.REACT_APP_ENV)
})()

export const FETCH_DATA_API = process.env.REACT_APP_ENV !== "dev" ? "/api/historical-data" : "/historical-data"
export const GET_HOLDINGS = process.env.REACT_APP_ENV !== "dev" ? "/api/portfolio/holdings" : "/portfolio/holdings"
export const PLACE_ORDER = process.env.REACT_APP_ENV !== "dev" ? "/api/order/place_order" : "/order/place_order"
export const GET_PROFILE = process.env.REACT_APP_ENV !== "dev" ? "/api/user/profile" : "/user/profile"
export const LOG_IN_API = process.env.REACT_APP_ENV !== "dev" ? "/api/user/login" : "/user/login"
export const LOG_OUT_API = process.env.REACT_APP_ENV !== "dev" ? "/api/user/logout" : "/user/logout"
export const SIGN_UP = process.env.REACT_APP_ENV !== "dev" ? "/api/user/register" : "/user/register"