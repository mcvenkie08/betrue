import _ from "lodash";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { getProfile, setModal, logout } from "../../../actions";

class SideMenuHeader extends React.Component {
  state = {
    activeItem: "portfolio",
    profileMenu: false,
  };

  componentDidMount() {
    this.props.getProfile();
  }

  onClickLogout = ()=>{
    this.props.logout()
  }

  onClickProfile = () => {
    const modal = {
      flag: !this.props.modalFlag,
      profile: this.props.profile,
    };
    if (!_.isEmpty(this.props.profile)) {
      console.log(this.props.profile);
      this.props.setModal(modal);
    }
  };

  onClickTab = (e) => {
    this.setState({
      ...this.state,
      activeItem: e.target.name,
    });
  };

  render() {
    return (
      <div className="ui large menu">
        <div className="right menu">
          <Link
            to="/orders"
            className={`${
              this.state.activeItem === "orders" ? "active" : ""
            } item`}
            name="orders"
            onClick={this.onClickTab}
          >
            Order
          </Link>
          <Link
            to="/"
            className={`${
              this.state.activeItem === "holdings" ? "active" : ""
            } item`}
            name="holdings"
            onClick={this.onClickTab}
          >
            Holdings
          </Link>
          <div
            className="item"
          >
            <text>{`Hey, ${this.props.user.username}`}</text>
          </div>
          <div class="ui menu">
            <div
              class={`ui pointing dropdown link item ${
                this.state.profileMenu ? "active visible" : ""
              }`}
              onClick={() =>
                this.setState({
                  ...this.state,
                  profileMenu: !this.state.profileMenu,
                })
              }

            >
              <img
                src="/resources/images/profile.png"
                style={{ maxWidth: 50 }}
              />
              <div
                class={`menu ${
                  this.state.profileMenu ? "transition visible" : ""
                }`}
              >
                <div class="item" onClick={this.onClickProfile}>Profile</div>
                <div class="item" onClick={this.onClickLogout}>Logout</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile.data,
    user: state.auth,
  };
};

export default connect(mapStateToProps, { getProfile, setModal, logout })(
  SideMenuHeader
);
