import jwtDecode from 'jwt-decode';
import {
  GET_HISTORIC_DATA,
  SET_DATE_RANGE,
  SET_SYMBOL,
  GET_HOLDINGS,
  PLACE_ORDER,
  SET_MODAL,
  GET_PROFILE,
  LOG_IN,
  LOG_OUT,
  SET_TOAST
} from "./types";
import history from "../history";
import { API } from "../utils/api";
import * as constants from "../utils/constants";

const onError = (dispatch, err) => {
  console.log(err);
  if (err.response) {
    console.log(err.response.data);
    if (err.response.status === 401) {
      dispatch({
        type: LOG_OUT,
        payload: {},
      });
      history.push("/login");
    }
  }
  if(err.message){
    setToast(dispatch,{message:err.message,type:'warn'})
  }
};

export const getHistoricData = (data) => async (dispatch, state) => {
  let response = "";
  console.log(data);
  try {
    response = await API.get(`${constants.FETCH_DATA_API}`, {
      params: data,
    });
    console.log(response.data);
    dispatch({
      type: GET_HISTORIC_DATA,
      payload: { data: response.data },
    });
  } catch (err) {
    console.log(err);
  }
};

export const register =(credentials) =>async(dispatch) =>{
  let response=""
  try {
    response = await API.post(`${constants.SIGN_UP}`, {
      name:credentials.name,
      username: credentials.username,
      password: credentials.password,
    });
    console.log(response.data);
    history.push("/");
  } catch (err) {
    onError(dispatch, err);
  }
}

export const loadRegister = ()=>{
  history.push('/register')
  return {
    type:'OnRegister',
    payload:''
  }
}

export const login = (credentials) => async (dispatch) => {
  let response = "";
  try {
    response = await API.post(`${constants.LOG_IN_API}`, {
      username: credentials.username,
      password: credentials.password,
    });
    console.log(response.data);
    const accessToken = response.data.access_token;
    //get user details from user api
    const { username } = jwtDecode(accessToken);
    const payload = {
      username,
    };
    dispatch({
      type: LOG_IN,
      payload,
    });
    history.push("/");
  } catch (err) {
    onError(dispatch, err);
  }
};

export const logout = () => async (dispatch) => {
  let response = "";
  try {
    response = await API.post(`${constants.LOG_OUT_API}`);
    console.log(response);
    dispatch({
      type: LOG_OUT,
      payload: {},
    });
    history.push('/')
  } catch (err) {
    console.log(err);
    onError(dispatch, err);
  }
};

export const setRangeDates = (dates) => {
  return {
    type: SET_DATE_RANGE,
    payload: dates,
  };
};

export const setSymbol = (symbol) => {
  return {
    type: SET_SYMBOL,
    payload: symbol,
  };
};

export const getHoldings = () => async (dispatch) => {
  let response = "";
  try {
    response = await API.get(`${constants.GET_HOLDINGS}`);
    console.log(response.data);
    dispatch({
      type: GET_HOLDINGS,
      payload: { data: response.data.data },
    });
  } catch (err) {
    onError(dispatch, err);
    console.log(err);
  }
};

export const placeOrder = (orderData) => async (dispatch) => {
  let response = "";
  console.log(orderData);
  try {
    response = await API.get(`${constants.PLACE_ORDER}`);
    console.log(response.data);
    dispatch({
      type: PLACE_ORDER,
      payload: { data: response.data.data },
    });
  } catch (err) {
    onError(dispatch, err);
    console.log(err);
  }
};

export const getProfile = () => async (dispatch) => {
  let response = "";
  try {
    response = await API.get(`${constants.GET_PROFILE}`);
    console.log(response.data);
    dispatch({
      type: GET_PROFILE,
      payload: { data: response.data.data },
    });
  } catch (err) {
    onError(dispatch, err);
    console.log(err);
  }
};

export const setModal = (modal) => {
  return {
    type: SET_MODAL,
    payload: modal,
  };
};

export const onClickBack = ()=>{
  history.goBack()
  return {
    type: 'ON_BACK',
    payload:''
  }
}

export const setToast = (dispatch,toast) =>{
  console.log(toast)
  dispatch({
    type:SET_TOAST,
    payload:toast
  });
  
}