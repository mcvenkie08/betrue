export const GET_HISTORIC_DATA="GET_HISTORIC_DATA"
export const SET_DATE_RANGE="SET_DATE_RANGE"
export const SET_SYMBOL="SET_SYMBOL"

export const GET_HOLDINGS="GET_HOLDINGS"

export const PLACE_ORDER="PLACE_ORDER"

export const SET_MODAL="SET_MODAL"

export const GET_PROFILE="GET_PROFILE"

export const LOG_IN="LOG_IN"
export const LOG_OUT="LOG_OUT"
export const SIGN_UP="SIGN_UP"

export const SET_TOAST="SET_TOAST"