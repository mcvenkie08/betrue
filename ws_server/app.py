import asyncio
import logging
from datetime import datetime
import websockets
import pathlib

cwd = pathlib.Path(__file__).parent.resolve()

logging.basicConfig(
    filename=f"{cwd}/ws.log",
    filemode="a",
    format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

class WebsocketServer:
    def __init__(self):
        self.clients = set()
        self.data = dict()

    async def register(self, ws) -> None:
        self.clients.add(ws)
        logging.info(f"{ws.remote_address} connects.")

    async def unregister(self, ws) -> None:
        self.clients.remove(ws)
        logging.info(f"{ws.remote_address} disconnects.")

    async def send_to_clients(self, action: str, message: str) -> None:
        logging.info(f"sending {action} results")
        if self.clients:
            await asyncio.wait([client.send(message) for client in self.clients])

    async def ws_handler(self, ws, uri: str) -> None:
        await self.register(ws)
        try:
            await self.distribute(ws)
        finally:
            await self.unregister(ws)

    async def distribute(self, ws) -> None:
        async for message in ws:
            await self.send_to_clients(message)


if __name__ == "__main__":
    ws = WebsocketServer()
    start_server = websockets.serve(ws.ws_handler, "0.0.0.0", 3213)
    print("Started")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
